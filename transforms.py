#Para conseguir la imagen original
from typing import List, Tuple
from PIL import Image

# Lee una imagen y devuelve la representación de la imagen como una matriz de tuplas RGB
def read_image(file_path: str) -> List[List[Tuple[int, int, int]]]:
    image = Image.open(file_path)
    pixels = list(image.getdata())
    width, height = image.size
    pixels = [pixels[i * width:(i + 1) * width] for i in range(height)]
    return pixels

# Guarda una matriz de tuplas RGB en un archivo de imagen
def save_image(image: List[List[Tuple[int, int, int]]], output_path: str) -> None:
    height = len(image)
    width = len(image[0])

    pixels = [pixel for row in image for pixel in row]

    image = Image.new("RGB", (width, height))
    image.putdata(pixels)
    image.save(output_path)

#Copia una imagen en matriz de tuplas y devuelve una copia
def copy_image(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    return [row[:] for row in image]

#Cambia los colores de la imagen
def change_colors(image: List[List[Tuple[int, int, int]]], to_change: Tuple[int, int, int], to_change_to: Tuple[int, int, int]) -> List[List[Tuple[int, int, int]]]:
    changed_colors_image = copy_image(image)

    for i in range(len(image)):
        for j in range(len(image[0])):
            if image[i][j] == to_change:
                changed_colors_image[i][j] = to_change_to

    return changed_colors_image

#Rota la imagen 90 grados a la derecha
def rotate_right(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    rotated_image = list(zip(*reversed(image)))
    rotated_image = [list(row) for row in rotated_image]
    return rotated_image

#Crea una imagen espejada verticalmente
def mirror(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    mirrored_image = image[::-1]
    return mirrored_image

#Rota los colores de la imagen sumando el incremento a cada componente RGB
def rotate_colors(image: List[List[Tuple[int, int, int]]], increment: int) -> List[List[Tuple[int, int, int]]]:
    #increment: Valor a sumar a cada componente RGB
    rotated_colors_image = [[((r + increment) % 256, (g + increment) % 256, (b + increment) % 256) for r, g, b in row] for row in image]
    return rotated_colors_image

#Aplica un efecto de desenfoque a la imagen original
def blur(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    rows, cols = len(image), len(image[0])
    blurred_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(1, rows - 1):
        for j in range(1, cols - 1):
            #Calcula valor medio de los vecinos
            neighbors = [
                image[i - 1][j],
                image[i + 1][j],
                image[i][j - 1],
                image[i][j + 1]
            ]
            blurred_pixel = (
                sum(p[0] for p in neighbors) // 4,
                sum(p[1] for p in neighbors) // 4,
                sum(p[2] for p in neighbors) // 4
            )
            blurred_image[i][j] = blurred_pixel

    return blurred_image

#Desplaza una imagen en los ejes horizontal y vertical
def shift(image: List[List[Tuple[int, int, int]]], horizontal: int, vertical: int) -> List[List[Tuple[int, int, int]]]:
    # horizontal: Número de píxeles a desplazar horizontalmente (positivo hacia la derecha, negativo hacia la izquierda).
    # vertical: Número de píxeles a desplazar verticalmente (positivo hacia arriba, negativo hacia abajo).
    rows, cols = len(image), len(image[0])
    shifted_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            new_i = (i + vertical) % rows
            new_j = (j + horizontal) % cols
            shifted_image[new_i][new_j] = image[i][j]

    return shifted_image

#Recorta una imagen para incluir solo los píxeles dentro de un rectángulo especificado
def crop(image: List[List[Tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> List[List[Tuple[int, int, int]]]:
    # x: Coordenada x de la esquina superior izquierda del rectángulo a recortar.
    # y: Coordenada y de la esquina superior izquierda del rectángulo a recortar.
    # width: Ancho del rectángulo a recortar.
    # height: Altura del rectángulo a recortar.
    cropped_image = [row[x:x+width] for row in image[y:y+height]]
    return cropped_image

# Convierte una imagen a escala de grises
def grayscale(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    grayscale_image = [[(int((r + g + b) / 3), int((r + g + b) / 3), int((r + g + b) / 3)) for r, g, b in row] for row in image]
    return grayscale_image

#Aplica un filtro a la imagen multiplicando cada componente RGB por un factor dado
def filter(image: List[List[Tuple[int, int, int]]], r: float, g: float, b: float) -> List[List[Tuple[int, int, int]]]:
    # r: Factor de multiplicación para el componente rojo.
    # g: Factor de multiplicación para el componente verde.
    # b: Factor de multiplicación para el componente azul.
    filtered_image = [[(min(int(pixel[0] * r), 255), min(int(pixel[1] * g), 255), min(int(pixel[2] * b), 255)) for pixel in row] for row in image]
    return filtered_image

def transform_image(image, func, *args):
    pass
def main():
    pass



