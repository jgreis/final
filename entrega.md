# ENTREGA CONVOCATORIA DE ENERO

Joana Gabriela Reis González (jg.reis.2023@alumnos.urjc.es)

Víde: https://youtu.be/wLT7lYRLGAI 

Requisitos mínimos implementados:
- Función change_colors
- Función rotate_right
- Función mirror
- Función rotate_colors
- Función blur
- Función shift
- Función crop
- Función grayscale
- Función filter
- Función main
- Programa transform_simple.py
- Programa transform_args.py
- Programa transform_multi.py
