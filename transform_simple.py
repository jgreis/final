#Acepta dos parámetros: el nombre de un fichero de imagen en formato RGB, y el nombre de una función de transformación entre las siguients:
    #rotate_right
    #mirror
    #blur
    #greyscale
# El progama lee la imagen, aplica la transformación indicada, y produce un fichero con el nombre y extensión del fichero de entrada, pero con el sufijo _trans.

import sys
import images
import transforms


def main():
    try:
        nombre = sys.argv[1]
    except IndexError:
        print("Error.... El nombre del archivo que has introducido no es un nombre de archivo.")
        sys.exit(1)
    try:
        algo = nombre.split(".jpg")
    except:
        print("Error.... El archivo que has introducido no tiene extension .jpg")
        sys.exit(1)

    opcion = sys.argv[2]
    image = images.read_img(nombre)

    if opcion == "rotate_right":
        imagen = transforms.rotate_right(image)
    elif opcion == "mirror":
        imagen = transforms.mirror(image)
    elif opcion == "blur":
        imagen = transforms.blur(image)
    elif opcion == "grayscale":
        imagen = transforms.grayscale(image)
    else:
        print("Error... El nombre de la función que has introducido es incorrecto. Intentalo de nuevo.")
        sys.exit(1)

    images.write_img(imagen, f"{algo[0]}_trans.jpg")


if __name__ == '__main__':
    main()