# Admite varios nombres de funciones de transformación, si es caso seguidos por sus parámetros. Aceptará los siguientes argumentos:
    # El primer argumento será la imagen a transformar.
    # El segundo argumento será la primera función de transformación.
    # El tercer argumento será, si es caso, sus argumentos.
    # El cuarto argumento será la segunda función de transformación.
    # y así sucesivamente.
# El programa lee la imagen, aplica las transformaciones indicadas, y produce la imagen resultante con el nombre y extensión del fichero de entrada, pero con el sufijo _trans en el nombre.

import sys
import images
import transforms


def main():
    var = 0

    try:
        nombre = sys.argv[1]
        var += 1
    except IndexError:
        print("Error.... El nombre del archivo que has introducido no es un nombre de archivo.")
        sys.exit(1)

    algo = nombre.split(".jpg")
    numero = algo.count("")
    if numero < 1:
        print("Error.... El archivo que has introducido no tiene extension .jpg")
        sys.exit(1)
    image = images.read_img(nombre)
    hola = len(sys.argv) - 1
    while var < hola:
        opcion = sys.argv.pop(2)
        var += 1

        if opcion == "change_colors":
            try:
                num1 = int(sys.argv.pop(2))
                num2 = int(sys.argv.pop(2))
                num3 = int(sys.argv.pop(2))
                num4 = int(sys.argv.pop(2))
                num5 = int(sys.argv.pop(2))
                num6 = int(sys.argv.pop(2))
            except IndexError:
                print("Error... Alguno de los valores que has introducido, no son correctos o no son numeros enteros. ")
                sys.exit(1)
            var += 6
            tup1 = (num1, num2, num3)
            tup2 = (num4, num5, num6)
            imagen = transforms.change_colors(image, tup1, tup2)

        elif opcion == "rotate_right":
            imagen = transforms.rotate_right(image)

        elif opcion == "mirror":
            imagen = transforms.mirror(image)

        elif opcion == "rotate_colors":
            try:
                incremento = int(sys.argv.pop(2))
            except IndexError:
                print("Error... Debes introducir un número para el incremento de colores.")
                sys.exit(1)
            var += 1
            imagen = transforms.rotate_colors(image, incremento)

        elif opcion == "blur":
            imagen = transforms.blur(image)

        elif opcion == "filter":
            try:
                num1 = float(sys.argv.pop(2))
                num2 = float(sys.argv.pop(2))
                num3 = float(sys.argv.pop(2))

            except IndexError:
                print("Error... Alguno de los valores que has introducido, no son numeros o con decimales. ")
                sys.exit(1)
            var += 3
            imagen = transforms.filter(image, num1, num2, num3)

        elif opcion == "crop":
            try:
                value_x = int(sys.argv.pop(2))
                value_y = int(sys.argv.pop(2))
                ancho = int(sys.argv.pop(2))
                alto = int(sys.argv.pop(2))

            except IndexError:
                print("Error... Faltan valores para recortar la imagen. Debes introducir x, y, ancho y alto.")
                sys.exit(1)
            var += 4
            imagen = transforms.crop(image, value_x, value_y, ancho, alto)

        elif opcion == "shift":
            try:
                horizontal = int(sys.argv.pop(2))
                vertical = int(sys.argv.pop(2))
            except IndexError:
                print("Error... Debes introducir valores enteros para el desplazamiento de la imagen.")
                sys.exit(1)
            var += 2
            imagen = transforms.shift(image, horizontal, vertical)

        elif opcion == "grayscale":
            imagen = transforms.grayscale(image)
        else:
            print("Error... El nombre de la función que has introducido es incorrecto. Intentalo de nuevo.")
            sys.exit(1)
        image = imagen
        images.write_img(imagen, f"{algo[0]}_trans.jpg")


if __name__ == '__main__':
    main()